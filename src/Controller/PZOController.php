<?php

namespace App\Controller;

use App\Entity\Bill;
use App\Entity\PZO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PZOController extends AbstractController
{
    /**
     * @Route("/pzo", name="pzo")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(PZO::Class)->findBy(['id' =>  5]);
        return $this->render('pzo/index.html.twig', [
            'controller_name' => 'PZOController',
            'repository' => $repository
        ]);
    }
}
