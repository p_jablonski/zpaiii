<?php

namespace App\Repository;

use App\Entity\WasteContainer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WasteContainer|null find($id, $lockMode = null, $lockVersion = null)
 * @method WasteContainer|null findOneBy(array $criteria, array $orderBy = null)
 * @method WasteContainer[]    findAll()
 * @method WasteContainer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WasteContainerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WasteContainer::class);
    }

    // /**
    //  * @return WasteContainer[] Returns an array of WasteContainer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WasteContainer
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
