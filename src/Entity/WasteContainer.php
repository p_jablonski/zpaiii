<?php

namespace App\Entity;

use App\Repository\WasteContainerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=WasteContainerRepository::class)
 */
class WasteContainer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dedicate;

    /**
     * @ORM\ManyToOne(targetEntity=local::class, inversedBy="WasteContainers")
     */
    private $local;

    /**
     * @ORM\OneToMany(targetEntity=Waste::class, mappedBy="waste_container")
     */
    private $wastes;

    /**
     * @ORM\ManyToOne(targetEntity=Dumpster::class, inversedBy="waste_container")
     */
    private $dumpster;

    public function __construct()
    {
        $this->wastes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getDedicate(): ?string
    {
        return $this->dedicate;
    }

    public function setDedicate(string $dedicate): self
    {
        $this->dedicate = $dedicate;

        return $this;
    }

    public function getLocal(): ?local
    {
        return $this->local;
    }

    public function setLocal(?local $local): self
    {
        $this->local = $local;

        return $this;
    }

    /**
     * @return Collection|Waste[]
     */
    public function getWastes(): Collection
    {
        return $this->wastes;
    }

    public function addWaste(Waste $waste): self
    {
        if (!$this->wastes->contains($waste)) {
            $this->wastes[] = $waste;
            $waste->setWasteContainer($this);
        }

        return $this;
    }

    public function removeWaste(Waste $waste): self
    {
        if ($this->wastes->removeElement($waste)) {
            // set the owning side to null (unless already changed)
            if ($waste->getWasteContainer() === $this) {
                $waste->setWasteContainer(null);
            }
        }

        return $this;
    }

    public function getDumpster(): ?Dumpster
    {
        return $this->dumpster;
    }

    public function setDumpster(?Dumpster $dumpster): self
    {
        $this->dumpster = $dumpster;

        return $this;
    }
}
