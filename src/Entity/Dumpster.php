<?php

namespace App\Entity;

use App\Repository\DumpsterRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity(repositoryClass=DumpsterRepository::class)
 */
class Dumpster
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street_number;

    /**
     * @ORM\OneToMany(targetEntity=WasteContainer::class, mappedBy="Dumpster")
     */
    private $waste_container;

    /**
     * @ORM\ManyToMany(targetEntity=PZO::class, mappedBy="Dumpster")
     */
    private $pZOs;

    public function __construct()
    {
        $this->waste_container = new ArrayCollection();
        $this->pZOs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->street_number;
    }

    public function setStreetNumber(string $street_number): self
    {
        $this->street_number = $street_number;

        return $this;
    }

    /**
     * @return Collection|wastecontainer[]
     */
    public function getWasteContainer(): Collection
    {
        return $this->waste_container;
    }

    public function addWasteContainer(wastecontainer $wasteContainer): self
    {
        if (!$this->waste_container->contains($wasteContainer)) {
            $this->waste_container[] = $wasteContainer;
            $wasteContainer->setDumpster($this);
        }

        return $this;
    }

    public function removeWasteContainer(wastecontainer $wasteContainer): self
    {
        if ($this->waste_container->removeElement($wasteContainer)) {
            // set the owning side to null (unless already changed)
            if ($wasteContainer->getDumpster() === $this) {
                $wasteContainer->setDumpster(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PZO[]
     */
    public function getPZOs(): Collection
    {
        return $this->pZOs;
    }

    public function addPZO(PZO $pZO): self
    {
        if (!$this->pZOs->contains($pZO)) {
            $this->pZOs[] = $pZO;
            $pZO->addDumpster($this);
        }

        return $this;
    }

    public function removePZO(PZO $pZO): self
    {
        if ($this->pZOs->removeElement($pZO)) {
            $pZO->removeDumpster($this);
        }

        return $this;
    }
}
