<?php

namespace App\Entity;

use App\Repository\PZORepository;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PZORepository::class)
 */
class PZO
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street_number;

    /**
     * @ORM\ManyToMany(targetEntity=Dumpster::class, inversedBy="pZOs")
     */
    private $dumpster;

    public function __construct()
    {
        $this->dumpster = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->street_number;
    }

    public function setStreetNumber(string $street_number): self
    {
        $this->street_number = $street_number;

        return $this;
    }

    /**
     * @return Collection|dumpster[]
     */
    public function getDumpster(): Collection
    {
        return $this->dumpster;
    }

    public function addDumpster(dumpster $dumpster): self
    {
        if (!$this->dumpster->contains($dumpster)) {
            $this->dumpster[] = $dumpster;
        }

        return $this;
    }

    public function removeDumpster(dumpster $dumpster): self
    {
        $this->dumpster->removeElement($dumpster);

        return $this;
    }
}
