<?php

namespace App\Controller;

use App\Entity\Bill;
use App\Entity\Local;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyAccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(): Response
    {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Bill::Class)->findBy(['local'=> 1]);
        return $this->render('my_account/index.html.twig', [
            'controller_name' => 'MyAccountController',
            'repository' => $repository
        ]);
    }
}
