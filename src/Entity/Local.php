<?php

namespace App\Entity;

use App\Repository\LocalRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=LocalRepository::class)
 */
class Local
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $local_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="Local")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Bill::class, mappedBy="Local")
     */
    private $bills;

    /**
     * @ORM\OneToMany(targetEntity=WasteContainer::class, mappedBy="Local")
     */
    private $wasteContainers;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->bills = new ArrayCollection();
        $this->wasteContainers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getLocalNumber(): ?string
    {
        return $this->local_number;
    }

    public function setLocalNumber(string $local_number): self
    {
        $this->local_number = $local_number;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setLocal($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getLocal() === $this) {
                $user->setLocal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Bill[]
     */
    public function getBills(): Collection
    {
        return $this->bills;
    }

    public function addBill(Bill $bill): self
    {
        if (!$this->bills->contains($bill)) {
            $this->bills[] = $bill;
            $bill->setLocal($this);
        }

        return $this;
    }

    public function removeBill(Bill $bill): self
    {
        if ($this->bills->removeElement($bill)) {
            // set the owning side to null (unless already changed)
            if ($bill->getLocal() === $this) {
                $bill->setLocal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WasteContainer[]
     */
    public function getWasteContainers(): Collection
    {
        return $this->wasteContainers;
    }

    public function addWasteContainer(WasteContainer $wasteContainer): self
    {
        if (!$this->wasteContainers->contains($wasteContainer)) {
            $this->wasteContainers[] = $wasteContainer;
            $wasteContainer->setLocal($this);
        }

        return $this;
    }

    public function removeWasteContainer(WasteContainer $wasteContainer): self
    {
        if ($this->wasteContainers->removeElement($wasteContainer)) {
            // set the owning side to null (unless already changed)
            if ($wasteContainer->getLocal() === $this) {
                $wasteContainer->setLocal(null);
            }
        }

        return $this;
    }
}
