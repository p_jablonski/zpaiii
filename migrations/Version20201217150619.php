<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201217150619 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bill (id INT AUTO_INCREMENT NOT NULL, local_id INT DEFAULT NULL, date_of_issue DATE NOT NULL, date_payment DATE NOT NULL, amount INT NOT NULL, account_number INT NOT NULL, INDEX IDX_7A2119E35D5A2101 (local_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dumpster (id INT AUTO_INCREMENT NOT NULL, street VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, street_number VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE local (id INT AUTO_INCREMENT NOT NULL, street VARCHAR(255) NOT NULL, local_number VARCHAR(255) NOT NULL, postcode VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pzo (id INT AUTO_INCREMENT NOT NULL, street VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, street_number VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pzo_dumpster (pzo_id INT NOT NULL, dumpster_id INT NOT NULL, INDEX IDX_E5AF713CF3C7AF7B (pzo_id), INDEX IDX_E5AF713CDE41E480 (dumpster_id), PRIMARY KEY(pzo_id, dumpster_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, local_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6495D5A2101 (local_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE waste (id INT AUTO_INCREMENT NOT NULL, waste_container_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, alt_name VARCHAR(255) DEFAULT NULL, INDEX IDX_2E76A48820E04725 (waste_container_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE waste_container (id INT AUTO_INCREMENT NOT NULL, local_id INT DEFAULT NULL, dumpster_id INT DEFAULT NULL, color VARCHAR(255) NOT NULL, dedicate VARCHAR(255) NOT NULL, INDEX IDX_33190DC55D5A2101 (local_id), INDEX IDX_33190DC5DE41E480 (dumpster_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bill ADD CONSTRAINT FK_7A2119E35D5A2101 FOREIGN KEY (local_id) REFERENCES local (id)');
        $this->addSql('ALTER TABLE pzo_dumpster ADD CONSTRAINT FK_E5AF713CF3C7AF7B FOREIGN KEY (pzo_id) REFERENCES pzo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pzo_dumpster ADD CONSTRAINT FK_E5AF713CDE41E480 FOREIGN KEY (dumpster_id) REFERENCES dumpster (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6495D5A2101 FOREIGN KEY (local_id) REFERENCES local (id)');
        $this->addSql('ALTER TABLE waste ADD CONSTRAINT FK_2E76A48820E04725 FOREIGN KEY (waste_container_id) REFERENCES waste_container (id)');
        $this->addSql('ALTER TABLE waste_container ADD CONSTRAINT FK_33190DC55D5A2101 FOREIGN KEY (local_id) REFERENCES local (id)');
        $this->addSql('ALTER TABLE waste_container ADD CONSTRAINT FK_33190DC5DE41E480 FOREIGN KEY (dumpster_id) REFERENCES dumpster (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pzo_dumpster DROP FOREIGN KEY FK_E5AF713CDE41E480');
        $this->addSql('ALTER TABLE waste_container DROP FOREIGN KEY FK_33190DC5DE41E480');
        $this->addSql('ALTER TABLE bill DROP FOREIGN KEY FK_7A2119E35D5A2101');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6495D5A2101');
        $this->addSql('ALTER TABLE waste_container DROP FOREIGN KEY FK_33190DC55D5A2101');
        $this->addSql('ALTER TABLE pzo_dumpster DROP FOREIGN KEY FK_E5AF713CF3C7AF7B');
        $this->addSql('ALTER TABLE waste DROP FOREIGN KEY FK_2E76A48820E04725');
        $this->addSql('DROP TABLE bill');
        $this->addSql('DROP TABLE dumpster');
        $this->addSql('DROP TABLE local');
        $this->addSql('DROP TABLE pzo');
        $this->addSql('DROP TABLE pzo_dumpster');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE waste');
        $this->addSql('DROP TABLE waste_container');
    }
}
