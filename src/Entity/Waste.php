<?php

namespace App\Entity;

use App\Repository\WasteRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 *
 * @ORM\Entity(repositoryClass=WasteRepository::class)
 */
class Waste
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"read","create"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"read","create"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alt_name;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $waste_container;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAltName(): ?string
    {
        return $this->alt_name;
    }

    public function setAltName(?string $alt_name): self
    {
        $this->alt_name = $alt_name;

        return $this;
    }

    public function getWasteContainer(): ?WasteContainer
    {
        return $this->waste_container;
    }

    public function setWasteContainer(?WasteContainer $waste_container): self
    {
        $this->waste_container = $waste_container;

        return $this;
    }
}
